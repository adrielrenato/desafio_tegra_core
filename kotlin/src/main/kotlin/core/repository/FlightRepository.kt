package core.repository

import core.model.Flight
import java.io.BufferedReader
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class FlightRepository {
    fun readFlights(uberAirCsvFile: BufferedReader, json99PlanesFile: BufferedReader): List<Flight> {
        var shortDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        var fullDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val flights: MutableList<Flight> = ArrayList()

        try {
            var i = 0

            uberAirCsvFile.forEachLine {
                if (i != 0) {
                    val line = it.split(",")
                    flights.add(Flight())
                    val lastFlightPosition = flights.size - 1

                    flights[lastFlightPosition].origem = line[1]
                    flights[lastFlightPosition].destino = line[2]
                    flights[lastFlightPosition].data = LocalDate.parse(line[3], shortDateFormatter)
                    flights[lastFlightPosition].saida = LocalDateTime.parse(line[3] + " " + line[4], fullDateFormatter)
                    flights[lastFlightPosition].chegada = LocalDateTime.parse(line[3] + " " + line[5], fullDateFormatter)
                    flights[lastFlightPosition].preco = line[6]
                    flights[lastFlightPosition].operadora = "99Planes"
                }

                i++
            }

            var flight: MutableList<String> =  mutableListOf<String>()
            json99PlanesFile.forEachLine {

                when {
                    it.contains("origem") -> flight.add(it.trim().replace("\"origem\": \"", "").replace("\",", ""))
                    it.contains("destino") -> flight.add(it.trim().replace("\"destino\": \"", "").replace("\",", ""))
                    it.contains("data_saida") -> flight.add(it.trim().replace("\"data_saida\": \"", "").replace("\",", ""))
                    it.contains("saida") -> flight.add(it.trim().replace("\"saida\": \"", "").replace("\",", ""))
                    it.contains("chegada") -> flight.add(it.trim().replace("\"chegada\": \"", "").replace("\",", ""))
                    it.contains("valor") -> flight.add(it.trim().replace("\"valor\": ", ""))
                    else -> flight = mutableListOf<String>()
                }

                if (flight.isNotEmpty() && flight.size == 6) {
                    flights.add(Flight())
                    val lastFlightPosition = flights.size - 1

                    flights[lastFlightPosition].origem = flight[0]
                    flights[lastFlightPosition].destino = flight[1]
                    flights[lastFlightPosition].data = LocalDate.parse(flight[2], shortDateFormatter)
                    flights[lastFlightPosition].saida = LocalDateTime.parse(flight[2] + " " + flight[3], fullDateFormatter)
                    flights[lastFlightPosition].chegada = LocalDateTime.parse(flight[2] + " " + flight[4], fullDateFormatter)
                    flights[lastFlightPosition].preco = flight[5]
                    flights[lastFlightPosition].operadora = "99Planes"
                }
            }
        } catch (exception: Exception) {
            println(exception)
        }

        var sortedList = flights.sortedWith(compareBy({ it.data }, { it.saida }, { it.chegada }))

        return sortedList;
    }

    fun filterFlight(flightList: List<Flight>, request: Flight): List<Flight> {
        val checkHasOrigin = flightList.filter { flight -> flight.origem == request.origem }
        val checkHasDestiny = flightList.filter { flight -> flight.destino == request.destino }

        when {
            checkHasOrigin.isEmpty() || checkHasDestiny.isEmpty() -> return flightList
        }

        val filteredFlightList: MutableList<Flight> = ArrayList()
        val directFlights: List<Flight> = flightList.filter { flight -> flight.origem == request.origem && flight.destino == request.destino && flight.data == request.data }

        when {
            // Verifica se o voo é direto.
            directFlights.isNotEmpty() -> {
                for (directFlight in directFlights) {
                    createNewFlight(filteredFlightList, directFlight, request)
                }
            }
        }

        val listFlightFromOriginAndDate = flightList.filter { flight -> flight.origem == request.origem && flight.data == request.data }

        when {
            // Verifica a origem do aeroporto escolhido, e se tem voo naquele dia.
            listFlightFromOriginAndDate.isNotEmpty() -> searchStopovers(flightList, listFlightFromOriginAndDate, request, filteredFlightList)
        }

        return when {
            filteredFlightList.isEmpty() -> flightList
            else -> filteredFlightList
        }
    }

    fun searchStopovers(flightList: List<Flight>, listFlightFromOriginAndDate: List<Flight>, request: Flight, filteredFlightList: MutableList<Flight>) {
        for (flightFromOriginAndDate in listFlightFromOriginAndDate) {
            val searchedStopovers = flightList.filter { flight -> flight.origem == flightFromOriginAndDate.destino && flight.destino == request.destino && flight.data >= flightFromOriginAndDate.data && flight.saida > flightFromOriginAndDate.chegada }

            // Irá procurar as escalas
            if (searchedStopovers.isNotEmpty()) {
                createNewFlight(filteredFlightList, flightFromOriginAndDate, request)
                var lastFlightPosition = filteredFlightList.size - 1
                var i = 1

                for (searchedStopover in searchedStopovers.withIndex()) {
                    // Só colocará a escala se caso estiver abaixo das 12 horas, ele colocará como uma escala
                    if (filteredFlightList[lastFlightPosition].trechos.size == 1 || filteredFlightList[lastFlightPosition].trechos[1].chegada.plusHours(12) > searchedStopover.value.saida) {
                        // Se caso ele já chegou ao seu destino, isso é um novo voo, não uma escala
                        if (filteredFlightList[lastFlightPosition].trechos.size != 1 && filteredFlightList[lastFlightPosition].trechos[1].destino == request.destino) {
                            val lastStopoverPosition = filteredFlightList[lastFlightPosition].trechos.size - 1
                            filteredFlightList[lastFlightPosition].chegada = filteredFlightList[lastFlightPosition].trechos[lastStopoverPosition].chegada

                            i = 1
                            createNewFlight(filteredFlightList, flightFromOriginAndDate, request)
                            lastFlightPosition = filteredFlightList.size - 1
                        }

                        filteredFlightList[lastFlightPosition].trechos.add(i, searchedStopover.value)
                        i++
                    }
                }

                val lastStopoverPosition = filteredFlightList[lastFlightPosition].trechos.size - 1
                filteredFlightList[lastFlightPosition].chegada = filteredFlightList[lastFlightPosition].trechos[lastStopoverPosition].chegada

                // Se caso só tiver a escala inicial, ele não mostrará, pois não será possível chegar no destino em apenas 12 horas
                if (filteredFlightList[lastFlightPosition].trechos.size == 1 && (filteredFlightList[lastFlightPosition].trechos[0].chegada != request.chegada || filteredFlightList[lastFlightPosition].trechos[0].saida != request.saida)) {
                    filteredFlightList.removeAt(lastFlightPosition)
                }

                continue
            }
        }
    }

    fun createNewFlight(filteredFlightList: MutableList<Flight>, flightFromOriginAndDate: Flight, request: Flight) {
        // Criará um novo voo
        filteredFlightList.add(Flight())
        val lastFlightPosition = filteredFlightList.size - 1

        filteredFlightList[lastFlightPosition].origem = request.origem
        filteredFlightList[lastFlightPosition].destino = request.destino
        filteredFlightList[lastFlightPosition].data = flightFromOriginAndDate.data
        filteredFlightList[lastFlightPosition].saida = flightFromOriginAndDate.saida
        filteredFlightList[lastFlightPosition].chegada = flightFromOriginAndDate.chegada
        filteredFlightList[lastFlightPosition].trechos.add(0, flightFromOriginAndDate)
    }
}