package core.controller

import org.springframework.web.bind.annotation.*
import core.model.Flight
import core.repository.FlightRepository
import java.io.*



@RestController
@RequestMapping(value = ["api/"])
class FlightController {

    @PostMapping(value = "/flight/list")
    fun listFlights(@RequestBody request: Flight): List<Flight> {
        val uberAirCsvFile: BufferedReader = File("src\\main\\assets\\uberair.csv").inputStream().bufferedReader()
        val json99PlanesFile: BufferedReader = File("src\\main\\assets\\99planes.json").inputStream().bufferedReader()

        return FlightRepository().filterFlight(FlightRepository().readFlights(uberAirCsvFile, json99PlanesFile), request)
    }
}