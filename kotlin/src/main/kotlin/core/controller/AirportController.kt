package core.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.File

@RestController
@RequestMapping(value = ["api/"])
class AirportController {
    @GetMapping(value = "/companies/list")
    fun listCompanies(): String {
        val listCompanies : String = File("src\\main\\assets\\aeroportos.json").inputStream().bufferedReader().use { it.readText() }

        return listCompanies
    }
}