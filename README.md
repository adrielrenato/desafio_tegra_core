# desafio_tegra_core

Requisitos não funcionais
- IDE (de preferência o Intellij)

Para rodar a aplicação siga os passos abaixo:

1 - Importe o projeto usando a model externa da Maven

2 - Após importar o projeto, execute o projeto no canto superior direito (Se caso não apareça vá no seguinte caminho /src/main/kotlin/core/KotlinApplication.kt, clique com o botão direito e clique em run para executar)

3 - Abra o postman e importe a collection para fazer as requisições. (Abra o postman clique em import e escolha o arquivo Desafio Tegra Backend.postman_collection)

4 - Pronto, o sistema está pronto para testes.

