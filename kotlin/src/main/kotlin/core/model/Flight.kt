package core.model

import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.collections.ArrayList

class Flight {
    var origem: String = ""
    var destino: String = ""
    var data: LocalDate = LocalDate.now()
    var saida: LocalDateTime = LocalDateTime.now()
    var chegada: LocalDateTime = LocalDateTime.now()
    var preco: String = ""
    var operadora: String? = null
    var trechos: MutableList<Flight> = ArrayList()
}